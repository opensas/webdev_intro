const express = require('express')
const path = require('path')

const app = express()

// app.use(express.text())
app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true })); // to support URL-encoded bodies

app.use(express.static(path.join(__dirname, 'public')))

const port = process.env.now ? 8080 : 5000

app.all('/hello', (req, res) => {
  res.send('hola mundo!')
})

app.all('/eco', (req, res) => {
  const result = {
    method: req.method,
    url: req.url,
    headers: req.headers,
    query: req.query,
    body: req.body
  }
  res.header("Content-Type", 'application/json')
  res.send(JSON.stringify(result, null, 2))
})

app.listen(port, () => {
  console.log(`Example app listening on port http://localhost:${port}!`)
})
