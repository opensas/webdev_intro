pushd ~/devel/apps/presentations/webdev_intro/live_demo > /dev/null
cp ../repsal_api/.nowignore ./live_api/
cp ../repsal_api/now.json ./live_api/

echo "copied .nowignore and now.json to ./live_api/"
popd > /dev/null
