pushd ~/devel/apps/presentations/webdev_intro/live_demo > /dev/null
cp -r ../repsal_spa/public/dist ./live_spa/public/dist
cp -r ../repsal_spa/public/favicon.png ./live_spa/public/favicon.png
cp -r ../repsal_spa/public/img ./live_spa/public/img

echo "copied data.js to ./live_api/services"
popd > /dev/null
