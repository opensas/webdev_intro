import fetch from 'node-fetch'
import fs from 'fs'

const main = async () => {

  // process params
  if (process.argv.length < 5) {
    console.error('bad parameters. usage:')
    console.error('  node --tls-min-v1.0 fetch.js <url> <from> <to>')
    console.error('')
    console.error('  url: url to be fetched. ej: https://myservice/api/users?p1=v1&page={{page}}&len=10')
    console.error('  from: number of first page to fetch')
    console.error('  to: number of last page to fetch')
    process.exit(1)
  }

  const url = process.argv[2]
  const from_page = parseInt(process.argv[3])
  const to_page = parseInt(process.argv[4])

  let data = []
  let page = from_page
  let page_data = []

  page_data = await fetch_page(url, page++)
  data = [...data, page_data]

  while (page <= to_page && page_data.length > 0) {
    if (page % 10 === 0)
      console.log(`procesando pagina ${page}/${to_page-from_page+1}`)
    page_data = await fetch_page(url, page++)
    data = [...data, page_data]
  }

  const json = JSON.stringify(data.flat(), null, 2)
  fs.writeFileSync('output.json', json, 'utf8')
}

const fetch_page = async (url, p) => {
  url = url.replace('{{page}}', p.toString())
  const response = await fetch(url)
  return await response.json()
}

main()