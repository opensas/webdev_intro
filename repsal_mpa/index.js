const express = require('express')
const path = require('path')

const app = express()

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true })); // to support URL-encoded bodies

const sanciones = require('./services/sanciones.js')

const port = process.env.now ? 8080 : 3000

app.all('/', (req, res) => {
  console.log(req.body)

  const b = req.body
  const data = sanciones.read(b.filter, b.order).map( element => {
    element.fecha_alta = element.fecha_alta.substring(0,10)
    return element
  })

  const template_data = {
    filter: b.filter,
    order: b.order,
    alert: !(b.alert === 'false'),
    sanciones: data,
  }
  template_data[`sorted_by_${b.order}`] = true

  console.log(template_data)

  res.render('index', template_data)
})

app.listen(port, () => {
  console.log(`Example app listening on port http://localhost:${port}!`)
})
