<!-- ---?image=assets/dr_js/img01.jpg -->
@snap[north text-07 span-100]
### Introducción al desarrollo de aplicaciones web
@snapend

@snap[midpoint span-100]
![web_development](assets/img/webdev/websites-development.png)
@snapend

@snap[south text-04 span-100]
Hablemos un poco de HTML, CSS, JavaScript, HTTP, APIs, REST, JSON, Node, Svelte, etc.
@snapend

---

@snap[north text-15]
### Agenda
@snapend

- HTTP: el _**protocolo**_ de la web

- HTML, CSS & JavaScript: los _**lenguajes**_ de la web

- **API** _versus_ **UI**

- Web services | JSON | REST

- **MPA** _versus_ **SPA**

- wsrepsal: un ejemplo práctico con [node](https://nodejs.org) & [svelte](https://svelte.dev/)

---

@snap[north text-07 span-100]
## primero las malas noticias...
@snapend

@snap[midpoint span-100 fragment]
![web_development](assets/img/webdev/roadmap.png)
@snapend

@snap[south text-08 span-100 fragment]
Sí... hay un [montón de cosas](https://roadmap.sh/assets/img/roadmaps/frontend-transparent.png) para aprender...
@snapend

---

@snap[north span-100]
### HTTP
@fa[quote-left quote-graphql](hyper text transfer protocol)
@snapend

@ul
- establece cómo se transmite la información en internet
- es un protocolo sin estado (no guarda ninguna información sobre conexiones anteriores)
- establece un esquema pedido-respuesta entre un cliente y un servidor
@ulend

---

@snap[north span-100]
#### HTTP: request & response
@snapend

@snap[midpoint span-90]
![HTTP request-response](assets/img/webdev/http/http_request_response.png)
@snapend

---

@snap[north span-100]
## HTTP request
@snapend

@snap[midpoint span-100]
![HTTP request](assets/img/webdev/http/http_request.png)
@snapend

---

@snap[north span-100]
## HTTP response
@snapend

@snap[midpoint span-90]
![HTTP response](assets/img/webdev/http/http_response.png)
@snapend

---

@snap[north span-100]
### HTML, CSS & JavaScript
@fa[quote-left quote-graphql](Los _**lenguajes**_ de la web)
@snapend

@snap[midpoint span-60]
![HTML, CSS y JavaScript](assets/img/webdev/html-css-and-javascript.png)
@snapend

@snap[south-west span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](HTML#Estructura)
@snapend

@snap[south span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](CSS#Presentación)
@snapend

@snap[south-east span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](JavaScript#Comportamiento)
@snapend

---

@snap[north span-100]
### HTML, CSS & JavaScript
@snapend

@snap[midpoint span-100]
![HTML, CSS & JavaScript](assets/img/webdev/html_css_js-michael_jackson.jpg)
@snapend

---

@snap[north span-100]
### Los lenguajes de la web: HTML
@fa[quote-left quote-graphql](hyper text markup language)
@snapend

@snap[midpoint span-100 text-05]

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>¿Qué es JavaScript?</title>
  </head>
  <body>
    <h1>¿Quién le teme a <i>JavaScript</i>?</h1>
    <p>
      Participante temeroso: <strong>desconocido</strong>
    </p>
  </body>
</html>
```
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://httpdemo.opensas.now.sh/ejemplo00.html) / [remix](https://thimble.mozilla.org/es/projects/533207/remix)
@snapend

---

@snap[north span-100]
#### Los lenguajes de la web: CSS
@fa[quote-left quote-graphql](cascading style sheet)
@snapend

@snap[midpoint span-100 text-05]

```html
<style>
  p {
    font-family: 'helvetica neue', helvetica, sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    text-align: center;
    border: 2px solid rgba(0,0,200,0.6);
    background: rgba(0,0,200,0.3);
    color: rgba(0,0,200,0.6);
    box-shadow: 1px 1px 2px rgba(0,0,200,0.4);
    border-radius: 10px;
    padding: 3px 10px;
    display: inline-block;
    cursor:pointer;
  }
</style>
```
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://httpdemo.opensas.now.sh/ejemplo00.html) / [remix](https://thimble.mozilla.org/es/projects/533207/remix)
@snapend

---

@snap[north span-100]
#### Los lenguajes de la web: JavaScript
@fa[quote-left](en realidad EcmaScript)
@snapend

@snap[midpoint span-100 text-06]

```html
<script>
  var parrafo = document.querySelector('p');

  parrafo.addEventListener('click', actualizarNombre);

  function actualizarNombre() {
    var nombre = prompt('¿Cómo te llamás?');
    if (nombre === '' || nombre === null) nombre = 'desconocido';
    parrafo.innerHTML = 'Participante temeroso: <strong>' + nombre + '</strong>';
  }
</script>
```
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://httpdemo.opensas.now.sh/ejemplo00.html) / [remix](https://thimble.mozilla.org/es/projects/533207/remix)
@snapend

---

@snap[north span-100]
## JavaScript !== Java
@snapend

@snap[midpoint span-100]
![HTML, CSS y JavaScript](assets/img/webdev/js_is_not_java.png)
@snapend

@snap[south text-08 span-100]
O sea... NO TIENEN NADA QUE VER!!!
@snapend

---

@snap[north span-100]
### API versus UI
@snapend

@ul[text-07]

- **web service**: un sistema de software corriendo en internet que permite intercambiar datos **entre aplicaciones**

- **API**: es un contrato mediante el cual una aplicación _servidor_ expone servicios para ser consumidos por una aplicación  _cliente_

- **UI**: es un conjunto de elementos que permite a un ser humano interactuar con un sistema informático

- **REST**: es un _estilo_ de servicio web que busca aprovechar la infraestructura de internet (_HTTP friendly_) y permitir intercambiar información de manera simple

- **JSON**: es un formato de texto sencillo y liviano para el intercambio de datos basado en los objectos de JavaScript

@ulend

---

@snap[north span-100]
### Los lenguajes de la web: REST!!!
@fa[quote-left quote-graphql](Representational State Transfer)
@snapend

<div class="text-06 fragment">
  <table>
    <thdead>
    <tr>
      <th>CRUD / ABM</th>
      <th>HTTP Verb</th>
      <th>URI</th>
      <th>Descripcion</th>
    </tr>
    </thdead>
    <tbody>
    <tr>
      <td>Read</td>
      <td>GET</td>
      <td>/api/sanciones</td>
      <td>Consulta sanciones</td>
    </tr>
    <tr>
      <td>Read</td>
      <td>GET</td>
      <td>/api/sanciones/{id}</td>
      <td>Consulta UNA sanción</td>
    </tr>
    <tr>
      <td>Update</td>
      <td>PUT</td>
      <td>/api/sanciones/{id}</td>
      <td>Actualiza una sanción</td>
    </tr>
    <tr>
      <td>Create</td>
      <td>POST</td>
      <td>/api/sanciones</td>
      <td>una nueva sanción</td>
    </tr>
    <tr>
      <td>Delete</td>
      <td>DELETE</td>
      <td>/api/sanciones/{id}</td>
      <td>Borra una sanción</td>
    </tr>
    </tbody>
  </table>
</div>

---

@snap[north span-100]
### Los lenguajes de la web: JSON
@fa[quote-left quote-graphql](hyper text markup language)
@snapend

@snap[midpoint span-100 text-05]

```json
[
  {
    "place_id": 13305093,
    "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
    "osm_type": "way",
    "osm_id": 212984836,
    "boundingbox": [
      "-34.6032944",
      "-34.6031944",
      "-58.371853516327",
      "-58.371753516326"
    ],
    "lat": "-34.6032444",
    "lon": "-58.3718035163265",
    "display_name": "348, Avenida Corrientes, Microcentro, Comuna 1, San Nicolás, Autonomous City of Buenos Aires, C1043AAQ, Argentina",
    "class": "place",
    "type": "house",
    "importance": 0.621
  }
]
```
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://nominatim.openstreetmap.org/search?format=json&q=Av.%20Corrientes%20348,%20Ciudad%20Aut%C3%B3noma%20de%20Buenos%20Aires,%20Argentina)
@snapend

---

@snap[north span-100]
### API versus UI: un ejemplo
@snapend

<table class="text-09">
  <tr>
    <th></th>
    <th>UI</th>
    <th>API</th>
  </tr>
  <tr>
    <td>aplicación</td>
    <td>[OpenStreetMap](https://www.openstreetmap.org/search?query=Av. Corrientes 348, Ciudad Autónoma de Buenos Aires, Argentina)</td>
    <td>[Nominatim](https://nominatim.openstreetmap.org/search?format=json&q=Av.%20Corrientes%20348,%20Ciudad%20Aut%C3%B3noma%20de%20Buenos%20Aires,%20Argentina) ([gmaps](https://www.google.com/maps/search/-34.6032444,-58.3718035163265))</td>
  </tr>
  <tr>
    <td>tipo</td>
    <td>página web</td>
    <td>servicio web</td>
  </tr>
  <tr>
    <td>usada por</td>
    <td>usuarios</td>
    <td>programas</td>
  </tr>
  <tr>
    <td>entrada</td>
    <td>caja de texto</td>
    <td>url (REST)</td>
  </tr>
  <tr>
    <td>salida</td>
    <td>mapa</td>
    <td>json</td>
  </tr>
</table>

---

@snap[north text-07 span-100]
## SPA versus MPA
@snapend

@snap[midpoint span-100]
![web_development](assets/img/webdev/spa_mpa/spa_mpa.png)
@snapend

@snap[south text-08 span-100]
Finalmente...
@snapend

---

@snap[north text-07 span-100]
## MPA lifecycle
@snapend

@snap[midpoint span-100]
![web_development](assets/img/webdev/spa_mpa/mpa_lifecycle.png)
@snapend

---

@snap[north span-100]
### MPA: Multi-Page Applications
@snapend

@ul[text-07]

- la página web entera se arma en el servidor
- cada interacción del usuario genera un nuevo request que vuelve a descargar toda la página
- para guardar el estado se recurre a cookies, inputs ocultos, sesiones en el servidor u otros mecanismos
- alto acoplamiento entre el frontend y el backend: a menudo se mezcla el código que accede a la base de datos, con el que maneja la lógica de negocios y el que arma la interfaz de usuario
- es difícil reutilizar el código si se desea agregar otro front-end

@ulend

---

@snap[north text-07 span-100]
## SPA lifecycle
@snapend

@snap[midpoint span-100]
![web_development](assets/img/webdev/spa_mpa/spa_lifecycle.png)
@snapend

---

@snap[north span-100]
### SPA: Single-Page Applications
@snapend

@ul[text-06]
- inicialmente descarga el código JavaScript que genera toda la UI en el cliente
- posteriormente no actualiza la página entera, sólo trae la información necesaria accediendo a servicios web REST
- el estado lo mantiene el código JavaScript que se ejecuta en el cliente
- mejora la experiencia de usuario reduciendo los _page refresh_
- reduce el acoplamiento entre el backend y el frontend
- nos obliga a desarrollar nuestras aplicaciones exponiendo toda la funcionalidad a través de una api
- permite reutilizar el código si se desea agregar otro front-end
- la inmensa mayoría de las aplicaciones web modernas son SPAs
@ulend

---

@snap[north text-07 span-100]
## Un caso práctico: repsal-MPA
@snapend

@snap[midpoint span-80]
![web_development](assets/img/webdev/repsal/repsal_ui.png)
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://repsalmpa.opensas.now.sh)
@snapend

---

@snap[north text-07 span-100]
#### Manteniendo el estado en un protocolo Stateless I
@snapend

@snap[midpoint span-80]
![web_development](assets/img/webdev/mpa_seclo/mpa_webforms_seclo.png)
@snapend

---

@snap[north text-07 span-100]
#### Manteniendo el estado en un protocolo Stateless II
@snapend

@snap[midpoint span-80]
![web_development](assets/img/webdev/mpa_seclo/mpa_webforms_seclo_2.png)
@snapend

---

@snap[north span-100]
## Let's Code a SPA app!
@snapend

@snap[midpoint shadow span-100]
![SIZE](assets/img/enough_talk.jpg)
@snapend

@snap[south text-06 span-100]
#### [demo online](https://repsalspa.opensas.now.sh/) | [repsal spa online](https://wsrepsal2.trabajo.gob.ar/) | [repsal api online](https://wsrepsal2.trabajo.gob.ar/api/sanciones?nombre_like=scarano)
@snapend

---

@snap[north span-100 text-06]
## Diseñando nuestra API REST
@snapend

@snap[midpoint span-100 text-05]

```json
$ curl -i -X GET /sanciones?page=1&len=10&filter=scarano&order=razon_social
[
  {
    "cuit": "30663839949",
    "razon_social": "FERNANDEZ POLLEDO SERGIO Y SCARANO ROBERTO S DE H",
    "fecha_alta": "2017-11-19T00:00:00.000Z",
    "tipo_infraccion": "Falta de Registro del Trabajador",
    "domicilio_relevamiento": "MITRE B. AV 2902   REMISERIA",
    "provincia": "BUENOS AIRES",
    "localidad": "MUNRO"
  },
  [...]
]

$ curl -i -X GET /sanciones/count?filter=scarano
{
  "count": 2
}
```
@snapend

@snap[south text-06 span-100]
#### [/api/sanciones endpoint](https://repsalapi.opensas.now.sh/sanciones?page=1&len=10&filter=scarano&order=razon_social) | [/api/sanciones/count endpoint](https://repsalapi.opensas.now.sh/sanciones/count?filter=scarano)
@snapend

---

@snap[north span-100]
### Node js
@snapend

@ul[text-08]
- es un runtime construido sobre el motor de JavaScript de Chrome
- permite ejecutar JavaScript en el servidor, fuera del browser
- usa un modelo *no-bloqueante*, donde todas las operaciones de *entrada/salida son asincrónicas*, lo cual le permite atender gran cantidad de pedidos de manera concurrente
- npm, el gestor de paquetes de node, es el ecosistema más grande de librerías libres del mundo
@ulend

---

@snap[north span-100]
### Node js - non-blocking IO
@snapend

@snap[midpoint span-100]
![web_development](assets/img/webdev/non_blocking_node.png)
@snapend

---

@snap[north span-100]
### Node js - asynchronous IO
@snapend

@snap[midpoint span-100]
![web_development](assets/img/webdev/non_blocking_node_2.png)
@snapend

---

@snap[north span-100 text-04]
## npm: node package manager
@fa[quote-left quote-graphql](el repositorio de código más popular de todos)
@snapend

@snap[midpoint span-90]
![web_development](assets/img/webdev/npm_node.png)
@snapend

---

@snap[north span-100]
### Quién usa Node js?
@snapend

@ul[text-09]
- Herramientas más populares: [stackshare.io top tools](https://stackshare.io/tools/top)
- Stacks que usan node: [stackshare.io](https://stackshare.io/nodejs) ([python](https://stackshare.io/python), [Java](https://stackshare.io/java), [Ruby](https://stackshare.io/ruby), [.net](https://stackshare.io/dot-net))
- Algunos usuarios reconocidos: [Uber](https://stackshare.io/uber-technologies/uber), [Netflix](https://stackshare.io/netflix/netflix), [Medium](https://stackshare.io/medium/medium-com), [Twitter](https://stackshare.io/twitter/twitter), [Wikipedia](https://stackshare.io/wikipedia/wikipedia), [MercadoLibre](https://stackshare.io/mercadolibre/mercadolibre), [PedidosYA](https://stackshare.io/pedidosya/pedidosya)
@ulend

---

@snap[north span-100]
## Let's Code a SPA app!
@snapend

@snap[midpoint shadow span-100]
![SIZE](assets/img/enough_talk.jpg)
@snapend

@snap[south text-06 span-100]
#### [demo online](https://repsalspa.opensas.now.sh/) | [repsal spa online](https://wsrepsal2.trabajo.gob.ar/) | [repsal api online](https://wsrepsal2.trabajo.gob.ar/api/sanciones?nombre_like=scarano)
@snapend

---

@snap[north span-100]
## Otros ejemplos de SPA
@snapend

@ul[text-09]

-  [Mapa Cultural](http://www.nardoz.com/mapa-cultura) ([slides](http://www.nardoz.com/mapa-cultura/slides/mediaparty_rendered.svg)): aplicación presentada en la MediaParty 2013

- [Twitter-cloud](https://twitter-cloud.opensas.now.sh/?users=mauriciomacri,alferdez,cfkargentina&interval=3&width=500&height=500&wordCount=30): visualización en tiempo real de palabras más twiteadas por los candidatos a presidente

@ulend


---?image=assets/img/gitpitch/gitpitch-audience.jpg

@snap[north span-100 h3-white]
### Preguntas?
@snapend

@fa[twitter text-white]&nbsp;[@opensas](https://twitter.com/opensas)

@fa[github text-white]&nbsp;[opensas](https://github.com/opensas)

@fa[gitlab text-white]&nbsp;[opensas](https://gitlab.com/opensas/svelte-todo)

@fa[chalkboard-teacher text-white]&nbsp;[slides](https://gitpitch.com/opensas/webdev_intro?grs=gitlab#/)

@fa[mobile-alt text-white]&nbsp;[buscador de repsal online](https://wsrepsal2.trabajo.gob.ar/)

@fa[rss text-white]&nbsp;[api de repsal online](https://wsrepsal2.trabajo.gob.ar/api/sanciones)
