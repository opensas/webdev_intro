slides: https://gitpitch.com/opensas/webdev_intro?grs=gitlab#/

# Presentación arquitectura de apliaciones web

intro genereal

roadmap.sh

# Roadmap



# Html, Css, JavaScript


Ejemplo html




## Qué es JavaScript

- animación lenguajes

-


### Quien usa node?

https://stackshare.io/tools/top


https://stackshare.io/nodejs



https://stackshare.io/mercadolibre/mercadolibre


https://stackshare.io/uber-technologies/uber

https://stackshare.io/netflix/netflix

https://stackshare.io/pedidosya/pedidosya


---

assets

https://codingninjas.co/blog/should-you-build-a-single-page-application-in-2018/

---

Ejemplo REST

| CRUD / ABM  | HTTP Verb | URI                 | Descripcion             |
| ----------- | --------- | ------------------- | ----------------------- |
| Read        | GET       | /api/sanciones      | Consulta sanciones      |
| Read single | GET       | /api/sanciones/{id} | Consulta UNA sanción    |
| Update      | PUT       | /api/sanciones/{id} | Actualiza una sanción   |
| Create      | POST      | /api/sanciones      | Crea una nueva sanción  |
| Delete      | DELETE    | /api/sanciones/{id} | Borra una sanción       |

--

Ejemplos api rest:

https://twitter-cloud.opensas.now.sh/?users=mauriciomacri,alferdez,cfkargentina&interval=3&width=500&height=500&wordCount=30

http://mediaparty-api.herokuapp.com/twitter/user_timeline_words?user=mauriciomacri&count=30

---

http://www.nardoz.com/mapa-cultura/main.html#0





http://devel.cartodb.com/api/v2/sql?q=select%20tipo%2C%20subtipo%2C%20nombre%2C%20direccion%2C%20telefono%2C%20email%2C%20web%2C%20lat%2C%20lon%20from%20cultura%20where%20(ST_Within(the_geom%2C%20ST_Envelope(ST_GeomFromText(%27LINESTRING(%20-58.412539656014296%20-34.589637956292144%2C%20-58.37307245030895%20-34.629840360709125%20)%27%2C%204326))))%20and%20(lower(nombre)%20like%20%27%25corrientes%25%27%20or%20lower(direccion)%20like%20%27%25corrientes%25%27)%20and%20((lower(subtipo)%20in%20(%27salas%20de%20cine%27%2C%20%27espacios%20incaa%27%2C%20%27salas%20teatrales%27)))

http://devel.cartodb.com/api/v2/sql?q=
  select tipo, subtipo, nombre, direccion, telefono, email, web, lat, lon from cultura where
    (ST_Within(the_geom, ST_Envelope(
        ST_GeomFromText('LINESTRING( -58.412539656014296 -34.589637956292144, -58.37307245030895 -34.629840360709125 )', 4326)))) and
    (lower(nombre) like '%corrientes%' or lower(direccion) like '%corrientes%') and
    ((lower(subtipo) in ('salas de cine', 'espacios incaa', 'salas teatrales')))

otro ejemplo API:

https://nominatim.openstreetmap.org/search?q=corrientes+348,buenos+aires,+argentina&format=json

https://nominatim.openstreetmap.org/search?q=corrientes+348,buenos+aires,+argentina

--

Servicio web:

Servicio que corren en la web que permite intercambiar datos entre aplicaciones

---

REST

Es un estilo de servicio que que busca aprovechar al infraestructura de internet y  permitir intercambiar información de una manera simple

---

JSON

JavaScrip Object Notation

Es un formato liviando para el intercambio de datos. Todo objeto json es a su vez un objecto válido en JavaScript, lo que hace que trabajar con él sea muy fácil.

---

UI versus API


Aplicación web | Servicio Web

Para ser usada por seres humanos | para ser usada por programas

UI - Interfaz de usuario | API - Interfaz de programación

--


code script:

```shell
$ mkdir live_api

$ cd live_api/

$ npm init

$ npm i express

$ npm i -D nodemon

```

create file index.js

copy now files and deploy


```shell
../copy_now_config.sh

now
```

create folder services

copy data.js

```shell
cp ../../repsal_api/services/data.js services/
```

copy .nowignore & now.json

```shell
cp ../../repsal_api/.nowignore . && cp ../../repsal_api/now.json .
```

deploy!!!

```shell
cp ../../repsal_api/.nowignore . && cp ../../repsal_api/now.json .
```

---

svelte spa

```shell
cd ..

npx degit sveltejs/template live_spa

cd live_spa

npm i
```
