<!-- ---?image=assets/dr_js/img01.jpg -->
@snap[north text-07 span-100]
### Introducción al desarrollo de aplicaciones web
@snapend

@snap[midpoint span-100]
![web_development](assets/img/webdev/websites-development.png)
@snapend

@snap[south text-04 span-100]
Hablemos un poco de HTML, CSS, JavaScript, HTTP, APIs, REST, JSON, Node, Svelte, etc...
@snapend

---

@snap[north text-15]
### Agenda
@snapend

- HTTP: el _**protocolo**_ de la web

- HTML, CSS & JavaScript: los _**lenguajes**_ de la web

- **API** _versus_ **UI**

- Web services | JSON | REST

- **MPA** _versus_ **SPA**

- wsrepsal: un ejemplo práctico con [node](https://nodejs.org) & [svelte](https://svelte.dev/)

---

@snap[north text-07 span-100]
## primero las malas noticias...
@snapend

@snap[midpoint span-100 fragment]
![web_development](assets/img/webdev/roadmap.png)
@snapend

@snap[south text-08 span-100 fragment]
Sí... hay un [montón de cosas](https://roadmap.sh/assets/img/roadmaps/frontend-transparent.png) para aprender...
@snapend

---

@snap[north span-100]
### HTTP
@fa[quote-left quote-graphql](hyper text transfer protocol)
@snapend

@ul
- establece cómo se transmite la información en internet
- es un protocolo sin estado (no guarda ninguna información sobre conexiones anteriores)
- establece un esquema pedido-respuesta entre un cliente y un servidor
@ulend

---

@snap[north span-100]
#### HTTP: request & response
@snapend

@snap[midpoint span-90]
![HTTP request-response](assets/img/webdev/http/http_request_response.png)
@snapend

---

@snap[north span-100]
## HTTP request
@snapend

@snap[midpoint span-100]
![HTTP request](assets/img/webdev/http/http_request.png)
@snapend

---

@snap[north span-100]
## HTTP response
@snapend

@snap[midpoint span-90]
![HTTP response](assets/img/webdev/http/http_response.png)
@snapend

---

@snap[north span-100]
### HTML, CSS & JavaScript
@fa[quote-left quote-graphql](Los _**lenguajes**_ de la web)
@snapend

@snap[midpoint span-60]
![HTML, CSS y JavaScript](assets/img/webdev/html-css-and-javascript.png)
@snapend

@snap[south-west span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](HTML#Estructura)
@snapend

@snap[south span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](CSS#Presentación)
@snapend

@snap[south-east span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](JavaScript#Comportamiento)
@snapend

---

@snap[north span-100]
### HTML, CSS & JavaScript
@snapend

@snap[midpoint span-100]
![HTML, CSS & JavaScript](assets/img/webdev/html_css_js-michael_jackson.jpg)
@snapend

---

@snap[north span-100]
### Los lenguajes de la web: HTML
@fa[quote-left quote-graphql](hyper text markup language)
@snapend

@snap[midpoint span-100 text-05]

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>¿Qué es JavaScript?</title>
  </head>
  <body>
    <h1>¿Quién le teme a <i>JavaScript</i>?</h1>
    <p>
      Participante temeroso: <strong>desconocido</strong>
    </p>
  </body>
</html>
```
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://httpdemo.opensas.now.sh/ejemplo00.html) / [remix](https://thimble.mozilla.org/es/projects/533207/remix)
@snapend

---

@snap[north span-100]
#### Los lenguajes de la web: CSS
@fa[quote-left quote-graphql](cascading style sheet)
@snapend

@snap[midpoint span-100 text-05]

```html
<style>
  p {
    font-family: 'helvetica neue', helvetica, sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    text-align: center;
    border: 2px solid rgba(0,0,200,0.6);
    background: rgba(0,0,200,0.3);
    color: rgba(0,0,200,0.6);
    box-shadow: 1px 1px 2px rgba(0,0,200,0.4);
    border-radius: 10px;
    padding: 3px 10px;
    display: inline-block;
    cursor:pointer;
  }
</style>
```
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://httpdemo.opensas.now.sh/ejemplo00.html) / [remix](https://thimble.mozilla.org/es/projects/533207/remix)
@snapend

---

@snap[north span-100]
#### Los lenguajes de la web: JavaScript
@fa[quote-left](en realidad EcmaScript)
@snapend

@snap[midpoint span-100 text-06]

```html
<script>
  var parrafo = document.querySelector('p');

  parrafo.addEventListener('click', actualizarNombre);

  function actualizarNombre() {
    var nombre = prompt('¿Cómo te llamás?');
    if (nombre === '' || nombre === null) nombre = 'desconocido';
    parrafo.innerHTML = 'Participante temeroso: <strong>' + nombre + '</strong>';
  }
</script>
```
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://httpdemo.opensas.now.sh/ejemplo00.html) / [remix](https://thimble.mozilla.org/es/projects/533207/remix)
@snapend

---

@snap[north span-100]
## JavaScript !== Java
@snapend

@snap[midpoint span-100]
![HTML, CSS y JavaScript](assets/img/webdev/js_is_not_java.png)
@snapend

@snap[south text-08 span-100]
O sea... NO TIENEN NADA QUE VER!!!
@snapend

---

@snap[north span-100]
### API versus UI
@fa[quote-left quote-graphql](algunos términos...)
@snapend

@ul[text-06]

- **web service**: un servicio corriendo en la red que permite intercambiar datos **entre aplicaciones**

- **REST**: es un _estilo_ de servicio web que busca aprovechar la infraestructura de internet y permitir intercambiar información de manera simple

- **API**: es un contrato mediante el cual una aplicación _servidor_ expone servicios para ser consumidos por una _cliente_. Típicamente el servidor y el cliente son aplicaciones

- **UI**: es un conjunto de elementos que permite a un ser humano interactuar con un sistema informático. Típicamente el cliente es el usuario final y servidor es una aplicación.

- **JSON**: es un formato de texto sencillo y liviano para el intercambio de datos. Es un subconjunto de la notación literal de objetos de JavaScript, y es independiente de cualquier lenguaje de programación.

@ulend

---

@snap[north span-100]
### Los lenguajes de la web: JSON
@fa[quote-left quote-graphql](hyper text markup language)
@snapend

@snap[midpoint span-100 text-05]

```json
[
  {
    "place_id": 13305093,
    "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
    "osm_type": "way",
    "osm_id": 212984836,
    "boundingbox": [
      "-34.6032944",
      "-34.6031944",
      "-58.371853516327",
      "-58.371753516326"
    ],
    "lat": "-34.6032444",
    "lon": "-58.3718035163265",
    "display_name": "348, Avenida Corrientes, Microcentro, Comuna 1, San Nicolás, Autonomous City of Buenos Aires, C1043AAQ, Argentina",
    "class": "place",
    "type": "house",
    "importance": 0.621
  }
]
```
@snapend

@snap[south text-06 span-100]
Ver ejemplo [en línea](https://nominatim.openstreetmap.org/search?format=json&q=Av.%20Corrientes%20348,%20Ciudad%20Aut%C3%B3noma%20de%20Buenos%20Aires,%20Argentina)
@snapend

---

@snap[north span-100]
### Los lenguajes de la web: REST!!!
@fa[quote-left quote-graphql](Representational State Transfer)
@snapend

<div class="text-06 fragment">
  <table>
    <thdead>
    <tr>
      <th>CRUD / ABM</th>
      <th>HTTP Verb</th>
      <th>URI</th>
      <th>Descripcion</th>
    </tr>
    </thdead>
    <tbody>
    <tr>
      <td>Read</td>
      <td>GET</td>
      <td>/api/sanciones</td>
      <td>Consulta sanciones</td>
    </tr>
    <tr>
      <td>Read</td>
      <td>GET</td>
      <td>/api/sanciones/{id}</td>
      <td>Consulta UNA sanción</td>
    </tr>
    <tr>
      <td>Update</td>
      <td>PUT</td>
      <td>/api/sanciones/{id}</td>
      <td>Actualiza una sanción</td>
    </tr>
    <tr>
      <td>Create</td>
      <td>POST</td>
      <td>/api/sanciones</td>
      <td>una nueva sanción</td>
    </tr>
    <tr>
      <td>Delete</td>
      <td>DELETE</td>
      <td>/api/sanciones/{id}</td>
      <td>Borra una sanción</td>
    </tr>
    </tbody>
  </table>
</div>

---

@snap[north span-100]
### API versus UI
@snapend

<table class="text-09">
  <tr>
    <th>Aplicación web</th>
    <th>Servicio web</th>
  </tr>
  <tr class="fragment">
    <td>usada por seres humanos</td>
    <td>usado por programas</td>
  </tr>
  <tr class="fragment">
    <td>UI - interfaz de usuario</td>
    <td>API - interfaz de programación</td>
  </tr>
  <tr class="fragment">
    <td>página web</td>
    <td>servicio web</td>
  </tr>
</table>
---

@snap[north span-100]
### API versus UI: un ejemplo
@snapend

<table class="text-09">
  <tr>
    <th></th>
    <th>UI</th>
    <th>API</th>
  </tr>
  <tr>
    <td>aplicación</td>
    <td>[OpenStreetMap](https://www.openstreetmap.org/search?query=Av. Corrientes 348, Ciudad Autónoma de Buenos Aires, Argentina)</td>
    <td>[Nominatim](https://nominatim.openstreetmap.org/search?format=json&q=Av.%20Corrientes%20348,%20Ciudad%20Aut%C3%B3noma%20de%20Buenos%20Aires,%20Argentina) ([gmaps](https://www.google.com/maps/search/-34.6032444,-58.3718035163265))</td>
  </tr>
  <tr>
    <td>entrada</td>
    <td>caja de texto</td>
    <td>url (REST)</td>
  </tr>
  <tr>
    <td>salida</td>
    <td>mapa</td>
    <td>json</td>
  </tr>
</table>

---

@snap[north text-07 span-100]
### Introducción al desarrollo de aplicaciones web
@snapend

@snap[midpoint span-100]
![web_development](assets/img/webdev/websites-development.png)
@snapend

@snap[south text-04 span-100]
Hablemos un poco de HTML, CSS, JavaScript, HTTP, APIs, REST, JSON, Node, Svelte, etc...
@snapend

---

@snap[north]
# Agenda2
@snapend

@snap[west span-50 text-center fragment]
### @fa[minus-circle text-orange] Library
@snapend

@snap[east span-50 text-center fragment]
### @fa[minus-circle text-orange] Framework
@snapend

@snap[south span-100 fragment]
## It's a Compiler!!!
@snapend

---

@snap[north span-50]
### About Me...
@snapend

@snap[west shadow span-33]
![IMAGE](assets/img/opensas_avatar.jpg)
@snapend

@snap[east span-60]
@ul[list-square-bullets list-spaced-bullets text-07 ](false)
- Engineer at UTN
- Developed systems at the ex-Ministry of Labor
- Supporter of free software and other freedoms
- Enjoy participating in the data and nerd communities
@ulend
@snapend

@snap[south]
twitter: [@opensas](https://twitter.com/opensas)
@snapend

---

@snap[north span-100]
### About Rich Harris...
@snapend

@snap[west shadow span-33]
![IMAGE](assets/img/rich_harris_avatar.jpg)
@snapend

@snap[east span-60]
@ul[list-square-bullets list-spaced-bullets text-07 ](false)
- Background...philosopher!
- Investigative journalist at The Guardian and the New York Times
- Creator of SvelteJs, Ractive y Rollup
- Conference Speaker
@ulend
@snapend

@snap[south]
twitter: [@rich_harris](https://twitter.com/rich_harris)
@snapend

---

@snap[north span-100]

### The Svelte Philosophy
@fa[quote-left quote-graphql](Do More With Less)

@snapend

@snap[midpoint shadow span-60]
![SVELTE](assets/img/svelte.png)
@snapend

@snap[south-west span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](Less Code#for the user)
@snapend

@snap[south span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](Less Code#for the developer)
@snapend

@snap[south-east span-30 text-08 fragment]
@box[rounded bg-orange text-white box-graphql](Less Code#to execute)
@snapend

---

@snap[north text-06 span-100]
### Less Code for the User... (size matters)
@snapend

@snap[south span-70]
![SIZE](assets/img/svelte_compared_size.png)
@snapend

@snap[south text-04 span-100]
source: [A RealWorld Comparison of Front-End Frameworks](https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/)
@snapend

---

@snap[north text-06 span-100]
### Less Code for Developers... (happiness matters)
@snapend

@snap[south span-70 fragment]
![SIZE](assets/img/svelte_compared_loc.png)
@snapend

@snap[south text-04 span-100]
source: [A RealWorld Comparison of Front-End Frameworks](https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/)
@snapend

---

@snap[north text-05 span-80]
### Less Code for Developers... (happiness matters)
@snapend

@snap[south-west text-04 span-50]
#### [working example](https://4bvtg.codesandbox.io/)
@snapend

@snap[west shadow fragment]
![REACT](assets/img/loc/react_loc.png)
@snapend

@snap[midpoint shadow fragment]
![VUE](assets/img/loc/vue_loc.png)
@snapend

@snap[east shadow fragment]
![SVELTE](assets/img/loc/svelte_loc.png)
@snapend

@snap[south-east text-04 span-100 fragment]
[React example](https://codesandbox.io/s/lesscode-sveltevsreeact-ujt7h) |
[Vue example](https://codesandbox.io/s/lesscode-sveltevsvue-sok3i) |
[Svelte example](https://codesandbox.io/s/lesscode-svelte-4bvtg)
@snapend

---

@snap[north span-100]
### Less Code for Developers
@snapend

@snap[midpoint text-07 span-80]
<blockquote>
<p>Death to boilerplate... How?</p>

<p>
Because Svelte is a compiler, we're not bound to the peculiarities of JavaScript: we can design a component authoring experience, rather than having to fit it around the semantics of the language. Paradoxically, this results in more idiomatic code — for example using variables naturally rather than via proxies or hooks — while delivering significantly more performant apps.
</p>
</blockquote>
@snapend

@snap[south text-04 span-100]
Source: [Death to boilerplate](https://svelte.dev/blog/write-less-code#Death_to_boilerplate)
@snapend
---

@snap[north span-100]
### Less Code to Execute
##### [Virtual DOM is pure overhead](https://rethinking-reactivity.surge.sh/#slide=9)
@snapend

@snap[midpoint text-06 span-80]
<br>
<blockquote>
<p>Why do frameworks use the virtual DOM then?</p>

<p>It's important to understand that virtual DOM isn't a feature. It's a means to an end, the end being declarative, state-driven UI development. Virtual DOM is valuable because it allows you to build apps without thinking about state transitions, with performance that is generally good enough.</p>

<p>But it turns out that we can achieve a similar programming model without using virtual DOM — and that's where Svelte comes in.</p>
</blockquote>
@snapend

@snap[south text-04 span-100]
Source: [Virtual DOM is pure overhead](https://svelte.dev/blog/virtual-dom-is-pure-overhead#Why_do_frameworks_use_the_virtual_DOM_then)
@snapend

---
@snap[north span-100]
### Serious Reactivity
@snapend

- [Rich Harris - Rethinking reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao) | [presentation](https://rethinking-reactivity.surge.sh)

- Mike Boston's [observable](https://rethinking-reactivity.surge.sh/#slide=19)

- [Destiny operator](https://rethinking-reactivity.surge.sh/#slide=20)

---

@snap[north span-100 text-09]
#### How to tell the computer that some state has changed?
##### With an API...
@snapend

```javascript
const { count } = this.state;
this.setState({
  count: count + 1
});
```

---

@snap[north span-100 text-09]
#### How to tell a computer that some state has changed?
##### Or better yet, without any API...
@snapend

```javascript
count += 1;
```

<br>
#### Since we're a compiler,<br>we can do that using "instrumentation"
<br>

```javascript
count += 1; $$invalidate('count', count);
```

---

@snap[north span-100]
## Lets Code!
@snapend

@snap[midpoint shadow span-100]
![SIZE](assets/img/enough_talk.jpg)
@snapend

@snap[south-east text-04 span-20]
#### [Ver app online](https://todo.opensas.now.sh/)
@snapend

---


@snap[north span-100]
### The Truth about Svelte
@snapend

@snap[west span-20 text-center fragment]
Not a Library
@snapend

@snap[midpoint span-20 text-center fragment]
Not a Framework
@snapend

@snap[east span-20 text-center fragment]
Not a Compiler
@snapend

@snap[south span-100 fragment]
#### It's a Language!
@snapend

@snap[south-west text-04 span-100]
source: [SvelteScript y Evan You](https://rethinking-reactivity.surge.sh/#slide=22)
@snapend

@snap[south-east text-04 span-100]
source: [What is Svelte](https://gist.github.com/Rich-Harris/0f910048478c2a6505d1c32185b61934)
@snapend

---

@snap[north span-100]
#### Svelte: a language for reactive interfaces
@snapend

@snap[south span-100]
@ul[list-spaced-bullets](false)
- Extend JavaScript transforming it into a reactive language, without breaking the language or existing tooling
- Instrument variable assignment and properties to make them **reactive**
- Add the '$:' statement to execute commands when the variables referenced change
@ulend
@snapend

@snap[south text-04 span-100]
source: [What is Svelte](https://gist.github.com/Rich-Harris/0f910048478c2a6505d1c32185b61934)
@snapend

---

@snap[north span-100]
#### Svelte: Learning Path
@snapend

- Watch the [Rethinking Reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao) presentation
- Follow the [Svelte Interactive Tutorial](https://svelte.dev/tutorial/basics)
- Buy the [Svelte Udemy Course](https://www.udemy.com/sveltejs-the-complete-guide/?couponCode=EARLY_YT) ($9.99!!!)
- See the [Sapper Framework](https://sapper.svelte.dev/)<br>The next small thing in web development
- See [Svelte Native](https://svelte-native.technology/)<br>The Svelte Mobile Development Experience

---?image=assets/img/gitpitch/gitpitch-audience.jpg

@snap[north span-100 h3-white]
### Questions?
@snapend

@fa[twitter text-white]&nbsp;[@opensas](https://twitter.com/opensas)

@fa[github text-white]&nbsp;[opensas](https://github.com/opensas)

@fa[gitlab text-white]&nbsp;[opensas](https://gitlab.com/opensas/svelte-todo)

@fa[chalkboard-teacher text-white]&nbsp;[slides](https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/)

@fa[list-alt text-white]&nbsp;[online demo app](https://todo.opensas.now.sh/)

---

### Thanks Everyone!

<br>

@fa[twitter]&nbsp;[@Rich_Harris](https://twitter.com/Rich_Harris) - Rich Harris

@fa[twitter]&nbsp;[@SvelteJs](https://twitter.com/SvelteJs) - The Svelte Community

@fa[twitter]&nbsp;[@tailwindcss](https://twitter.com/tailwindcss) - tailwindcss

@fa[twitter gp-contact]&nbsp;[@gitpitch](https://twitter.com/gitpitch) - David Rusell

@fa[twitter]&nbsp;[@gvilarino](https://twitter.com/gvilarino) - Guido Vilariño
