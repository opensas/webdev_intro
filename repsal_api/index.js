const express = require('express')
const app = express()

const sanciones = require('./services/sanciones.js')

const port = process.env.now ? 8080 : 3000

app.get('/', (req, res) => {
  console.log(`got a request from ${req.headers['user-agent']}!!!`)
  res.send('Hello World!')
})

app.get('/upper/:word', (req, res) => {
  res.send({ result: req.params.word.toUpperCase()})
})

app.get('/sanciones', (req, res) => {
  const q = req.query
  const data = sanciones.read(q.filter, q.order, q.page, q.len)
  res.send(data)
})

app.get('/sanciones/count', (req, res) => {
  const data = sanciones.count(req.query.filter)
  res.send(data)
})

app.listen(port, () => {
  console.log(`Example app listening on port http://localhost:${port}!`)
})
