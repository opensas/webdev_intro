const data = require('./data.js')

exports.read = (filter, order, page = 1, len = 5) => {
  let result = data
  if (filter) result = result.filter( s => s.razon_social.toUpperCase().includes(filter.toUpperCase()))

  if (order) result = result.sort( (s1, s2) =>
    s1[order] === s2[order] ? 0 : (s1[order] > s2[order] ? 1 : -1)
  )

  len = parseInt(len)
  const from = (parseInt(page)-1) * len
  result = result.slice(from, from + len)

  return result
}

exports.count = (filter) => {
  let result = data
  if (filter) result = result.filter( s => s.razon_social.toUpperCase().includes(filter.toUpperCase()))
  return { count: result.length }
}
